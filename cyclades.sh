#!/bin/bash

echo "Which row do you want to connect to?"

read row

valid_row='^[1-3]|5$'

while ! [[ $row =~ $valid_row ]]
do
  echo "That is not a valid row number.  Pick a row between 1 and 3"
  read row
done


if [ $row -lt 4 ]
    then
echo "Alright, let's connect to row $row

External racks 1 - 20 = port 1 - 20
Internal racks 1 - 20 = port 25 - 44
"
fi  

if [ $row == 1 ]
    then
    echo "
Row 1 Internal AGG CORE = port 45 - 46
Row 1 External AGG CORE = port 47 - 48
Row 1 ASA = port 21
Row 1 Urban 4948 = port 22
Row 1 Urban ASR = port 23
Row 1 Urban NULL Router = port 24
"
elif [ $row == 2 ]
    then
    echo "
Row 2 Internal AGG CORE = port 45 - 46
Row 2 External AGG CORE = port 47 - 48
"
elif [ $row == 3 ]
    then
    echo "
Row 3 Internal AGG CORE = Port 45 - 46
Row 3 External AGG CORE = Port 47 - 48
Row 3 Bell ASR = port 24
"

elif [ $row == 5 ]
    then
    echo "
van-esd-r5r11-acsw-1 = Port 1
van-esd-r5r12-acsw-1 = Port 2
van-esd-r5r13-acsw-1 = Port 3
van-esd-r5r14-tusw-1 = Port 4
van-esd-r5r14-tusw-2 = Port 5
van-esd-r5r14-tusw-3 = Port 6
van-esd-r5r15-tusw-1 = Port 7
van-esd-r5r15-tusw-2 = Port 8
van-esd-r5r15-tusw-3 = Port 9
van-esd-r5r16-mcsw-1 = Port 10
van-esd-r5r16-mcsw-2 = Port 11
van-esd-r5r16-mcsw-3 = Port 12
van-esd-r5r17-mcsw-1 = Port 13
van-esd-r5r17-mcsw-2 = Port 14
van-esd-r5r17-mcsw-3 = Port 15
van-esd-r5r18-mcsw-1 = Port 16
van-esd-r5r18-mcsw-2 = Port 17
van-esd-r5r19-acsw-1 = Port 18
van-esd-r5r19-acsw-2 = Port 19

"

fi

echo "Which port do you want to connect to?"

read port

if [ $port -lt 10 ]
    then
    port="0$port"
fi

if [ $row == 5 ]
    then
    ssh Admin@10.11.1.222 -p 31$port
else
    ssh -t -22 root:70$port@10.101.$row.248
fi
